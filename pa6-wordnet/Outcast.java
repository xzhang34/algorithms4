import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;
/*
 * Outcast detection. Given a list of wordnet nouns A1, A2, ..., An,
 * which noun is the least related to the others?
 * To identify an outcast, compute the sum of the distances between each noun
 * and every other one:
 * di   =   dist(Ai, A1)   +   dist(Ai, A2)   +   ...   +   dist(Ai, An)
 * and return a noun At for which dt is maximum
 */
public class Outcast {
	private WordNet wordnet;
	public Outcast(WordNet wordnet) {
		// constructor takes a WordNet object
		if (wordnet == null)
			throw new java.lang.IllegalArgumentException("null argument");
		this.wordnet = wordnet;
	}
	/* di   =   dist(Ai, A1)   +   dist(Ai, A2)   +   ...   +   dist(Ai, An) */
	/* return a noun At for which dt is maximum */
	public String outcast(String[] nouns) {
		// given an array of WordNet nouns, return an outcast
		if (nouns == null || nouns.length == 0)
			throw new java.lang.IllegalArgumentException("null argument");
		int max = 0;
		int max_pos = 0;
		int di;
		for (int i = 0; i < nouns.length; i++) {
			di = 0;
			for (int k = 0; k < nouns.length; k++) {
				if (k != i)
					di += wordnet.distance(nouns[i], nouns[k]);
			}
			if (di > max) {
				max = di;
				max_pos = i;
			}
		}
		return nouns[max_pos];
	}

	public static void main(String[] args) {
		// see test client below
		StdOut.println(args[0] + " - " + args[1]);
	    WordNet wordnet = new WordNet(args[0], args[1]);
	    Outcast outcast = new Outcast(wordnet);
	    for (int t = 2; t < args.length; t++) {
	        In in = new In(args[t]);
	        String[] nouns = in.readAllStrings();
	        StdOut.println(args[t] + ": " + outcast.outcast(nouns));
	    }
	}
}
