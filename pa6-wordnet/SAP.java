import edu.princeton.cs.algs4.BreadthFirstDirectedPaths;
import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.ST;

public class SAP {
	private Digraph G;
	/* Implement a software cache of recently computed length() and ancestor() queries.*/
	private ST<String, Integer> lenCache = new ST<String, Integer>();
	private ST<String, Integer> ansCache = new ST<String, Integer>();

	// constructor takes a digraph (not necessarily a DAG)
	public SAP(Digraph G) {
		if (G == null)
			throw new java.lang.IllegalArgumentException("null argument");
		this.G = new Digraph(G);
	}

	// length of shortest ancestral path between v and w; -1 if no such path
	public int length(int v, int w) {
		String key = v + "-" + w;
		if (lenCache.contains(key)) {
			int len = lenCache.get(key);
			lenCache.delete(key);
			return len;
		}

		BreadthFirstDirectedPaths bfsv = new BreadthFirstDirectedPaths(G, v);
		BreadthFirstDirectedPaths bfsw = new BreadthFirstDirectedPaths(G, w);

		int ancestor = ancestor(v, w);
		int len = -1;
		if (ancestor != -1)
			len = bfsv.distTo(ancestor) + bfsw.distTo(ancestor);
		lenCache.put(key, len);
		return len;
	}

	// a common ancestor of v and w that participates in a shortest ancestral path; -1 if no such path
	public int ancestor(int v, int w) {
		String key = v + "-" + w;
		if (ansCache.contains(key)) {
			int ans = ansCache.get(key);
			ansCache.delete(key);
			return ans;
		}

		BreadthFirstDirectedPaths bfsv = new BreadthFirstDirectedPaths(G, v);
		BreadthFirstDirectedPaths bfsw = new BreadthFirstDirectedPaths(G, w);

		int sa = -1;
		int saPath = Integer.MAX_VALUE;

		Queue<Integer> ancestors = new Queue<>();

		for (int i = 0; i < G.V(); i++) {
			if (bfsv.hasPathTo(i) && bfsw.hasPathTo(i))
				ancestors.enqueue(i);
		}

		for (Integer parent : ancestors) {
			int v_parent = bfsv.distTo(parent);
			int w_parent = bfsw.distTo(parent);
			if (v_parent + w_parent < saPath) {
				saPath = v_parent + w_parent;
				sa = parent;
			}
		}
		ansCache.put(key, sa);
		return sa;
	}

	// length of shortest ancestral path between any vertex in v and any vertex in w; -1 if no such path
	public int length(Iterable<Integer> v, Iterable<Integer> w) {
		if (v == null || w == null)
			throw new java.lang.IllegalArgumentException();

		String key = v.toString() + "_" + w.toString();
		if (lenCache.contains(key)) {
			int len = lenCache.get(key);
			lenCache.delete(key);
			return len;
		}
		BreadthFirstDirectedPaths bfsv = new BreadthFirstDirectedPaths(G, v);
		BreadthFirstDirectedPaths bfsw = new BreadthFirstDirectedPaths(G, w);

		int ancestor = ancestor(v, w);
		int len = -1;
		if (ancestor != -1)
			len = bfsv.distTo(ancestor) + bfsw.distTo(ancestor);
		lenCache.put(key, len);
		return len;
	}

	// a common ancestor that participates in shortest ancestral path; -1 if no such path
	public int ancestor(Iterable<Integer> v, Iterable<Integer> w) {
		if (v == null || w == null)
			throw new java.lang.IllegalArgumentException();

		String key = v.toString() + "_" + w.toString();
		if (ansCache.contains(key)) {
			int ans = ansCache.get(key);
			ansCache.delete(key);
			return ans;
		}

		BreadthFirstDirectedPaths bfsv = new BreadthFirstDirectedPaths(G, v);
		BreadthFirstDirectedPaths bfsw = new BreadthFirstDirectedPaths(G, w);

		int sa = -1;
		int saPath = Integer.MAX_VALUE;

		Queue<Integer> ancestors = new Queue<>();

		for (int i = 0; i < G.V(); i++) {
			if (bfsv.hasPathTo(i) && bfsw.hasPathTo(i))
				ancestors.enqueue(i);
		}

		for (Integer parent : ancestors) {
			int v_parent = bfsv.distTo(parent);
			int w_parent = bfsw.distTo(parent);
			if (v_parent + w_parent < saPath) {
				saPath = v_parent + w_parent;
				sa = parent;
			}
		}
		ansCache.put(key, sa);
		return sa;
	}

	// do unit testing of this class
	public static void main(String[] args) {
	    In in = new In(args[0]);
	    Digraph G = new Digraph(in);
	    StdOut.printf("test here ", args[0]);
	    SAP sap = new SAP(G);
	    while (!StdIn.isEmpty()) {
	        int v = StdIn.readInt();
	        int w = StdIn.readInt();
	        int length   = sap.length(v, w);
	        int ancestor = sap.ancestor(v, w);
	        StdOut.printf("length = %d, ancestor = %d\n", length, ancestor);
	    }
	}
}
