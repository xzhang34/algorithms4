/*
 * The WordNet digraph. Your first task is to build the wordnet digraph:
 * each vertex v is an integer that represents a synset, and each directed edge v-w represents
 * that w is a hypernym of v. The wordnet digraph is a rooted DAG: it is acyclic and has one vertex-the
 * root-that is an ancestor of every other vertex.
 * However, it is not necessarily a tree because a synset can have more than one hypernym.
 * A small subgraph of the wordnet digraph is illustrated below.
 */
/*
 * Corner cases.
 * All methods and the constructor should throw a java.lang.IllegalArgumentException if any argument is null.
 * The constructor should throw a java.lang.IllegalArgumentException if the input does not correspond
 * to a rooted DAG. The distance() and sap() methods should throw a java.lang.IllegalArgumentException
 * unless both of the noun arguments are WordNet nouns.
 */

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.ST;
import edu.princeton.cs.algs4.Bag;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.DirectedCycle;

public class WordNet {
	private Digraph wordnet;
	/* a noun appear in more than one synset */
	private ST<String, Bag<Integer>> nouns;
	private ST<Integer, String> synset;
	private SAP sap;

	// constructor takes the name of the two input files
	public WordNet(String synsets, String hypernyms) {
		/*
		 * The file synsets.txt lists all the (noun) synsets in WordNet.
		 * The first field is the synset id (an integer), the second field is the synonym set
		 * (or synset), and the third field is its dictionary definition (or gloss).
		 */
		if (synsets == null || hypernyms == null)
				throw new java.lang.IllegalArgumentException("null input");

		synset = new ST<Integer, String>();
		nouns = new ST<String, Bag<Integer>>();
		In syn = new In(synsets);
		while (!syn.isEmpty()) {
			String line = syn.readLine();
			String[] tokens = line.split(",");
			int id = Integer.parseInt(tokens[0]);
			String word = tokens[1];
			synset.put(id, word);
			String[] list = word.split(" ");
			Bag<Integer> idList = null;
			for (String noun : list) {
				if (nouns.contains(noun)) {
					idList = nouns.get(noun);
				} else {
					idList = new Bag<Integer>();
				}
				idList.add(id);
				nouns.put(noun, idList);
			}
		}
		/*
		 * The file hypernyms.txt contains the hypernym relationships:
		 * The first field is a synset id;
		 * subsequent fields are the id numbers of the synset's hypernyms.
		 */
		wordnet = new Digraph(synset.size());
		In hyper = new In(hypernyms);
		while (!hyper.isEmpty()) {
			String line = hyper.readLine();
			String[] tokens = line.split(",");
			int id = Integer.parseInt(tokens[0]);
			for (int i = 1; i < tokens.length; i++) {
				wordnet.addEdge(id, Integer.parseInt(tokens[i]));
			}
		}
		/* if the input does not correspond to a rooted DAG */
		DirectedCycle cycle = new DirectedCycle(wordnet);
		if (cycle.hasCycle())
			throw new java.lang.IllegalArgumentException("non DAG");
		int numberRoot = 0;
		for (int i = 0; i < wordnet.V(); i++) {
			int count = 0;
			for (int v : wordnet.adj(i))
				count++;

			if (count == 0)
				numberRoot++;
		}

		if (numberRoot != 1)
			throw new java.lang.IllegalArgumentException("non DAG");

		sap = new SAP(wordnet);
	}

	// returns all WordNet nouns
	public Iterable<String> nouns() {
		return nouns.keys();
	}

	// is the word a WordNet noun?
	public boolean isNoun(String word) {
		if (word == null)
			throw new java.lang.IllegalArgumentException("null input");
		return nouns.contains(word);
	}

	// distance between nounA and nounB (defined below)
	public int distance(String nounA, String nounB) {
		if (nounA == null || nounB == null)
			throw new java.lang.IllegalArgumentException("null input");
		Bag<Integer> a = nouns.get(nounA);
		Bag<Integer> b = nouns.get(nounB);
		return sap.length(a, b);
	}

	// a synset (second field of synsets.txt) that is the common ancestor of nounA and nounB
	// in a shortest ancestral path (defined below)
	public String sap(String nounA, String nounB) {
		if (nounA == null || nounB == null)
			throw new java.lang.IllegalArgumentException("null input");
		Bag<Integer> a = nouns.get(nounA);
		Bag<Integer> b = nouns.get(nounB);
		return synset.get(sap.ancestor(a, b));
	}

	// do unit testing of this class
	public static void main(String[] args) {
		WordNet wordNet1 = new WordNet("synsets3.txt", "hypernyms3InvalidTwoRoots.txt");
	    WordNet wordNet = new WordNet("synsets.txt", "hypernyms.txt");
	    String nounA = "flair";
	    String nounB = "sum";
	    StdOut.println("length " +  wordNet.distance(nounA, nounB));
	    StdOut.println("chang-thin length " +  wordNet.distance("change", "thing"));
	}
}
