/*
 * Dequeue. A double-ended queue or deque (pronounced "deck") is a generalization of a stack and a queue
 * that supports adding and removing items from either the front or the back of the data structure.
 *
 * Performance requirements.
 * Deque implementation must support each deque operation in constant worst-case time
 * and use space proportional to the number of items currently in the deque.
 * Additionally, your iterator implementation must support each operation
 * (including construction) in constant worst-case time.
 */

import java.util.Iterator;
import edu.princeton.cs.algs4.StdOut;


public class Deque<Item> implements Iterable<Item> {
	private Node first, last;
	private int size;

	private class Node {
		private Item item;
		private Node next;
		private Node prev;
	};

	public Deque() {
		// construct an empty deque
		first = null;
		last = null;
	}

	public boolean isEmpty() {
		// is the deque empty?
		return (first == null) && (last == null);
	}

	public int size() {
		// return the number of items on the deque
		return size;
	}

	public void addFirst(Item item) {
		// add the item to the front
		validateInput(item);

		Node oldfirst = first;
		first = new Node();
		first.item = item;
		if (oldfirst == null && last == null)
			last = first;
		else {
			first.next = oldfirst;
			oldfirst.prev = first;
		}
		size += 1;
	}

	public void addLast(Item item) {
		// add the item to the end
		validateInput(item);

		Node oldlast = last;
		last = new Node();
		last.item = item;
		if (oldlast == null && first == null)
			first = last;
		else {
			oldlast.next = last;
			last.prev = oldlast;
		}
		size += 1;
	}

	public Item removeFirst() {
		// remove and return the item from the front
		if (isEmpty())
			throw new java.util.NoSuchElementException();

		Node oldfirst = first;
		Item p = oldfirst.item;
		if (first == last) {
			first = null;
			last = null;
			size = 0;
		} else {
			first = first.next;
			size -= 1;
		}
		oldfirst.prev = null;
		oldfirst.next = null;
		oldfirst.item = null;
		return p;
	}

	public Item removeLast() {
		// remove and return the item from the end
		if (isEmpty())
			throw new java.util.NoSuchElementException();

		Node oldlast = last;
		Item p = oldlast.item;
		if (first == last) {
			first = null;
			last = null;
			size = 0;
		} else {
			last = last.prev;
			size -= 1;
		}
		oldlast.prev = null;
		oldlast.next = null;
		oldlast.item = null;
		return p;
	}

	@Override
	public Iterator<Item> iterator() {
		// return an iterator over items in order from front to end
		return new DequeIterator();
	}

	private class DequeIterator implements Iterator<Item> {
		private Node current = first;

		@Override
		public boolean hasNext() {
			return current != null && current != last.next;
		}

		@Override
		public void remove() {
			throw new java.lang.UnsupportedOperationException();
		}

		@Override
		public Item next() {
			if (current == null)
				throw new java.util.NoSuchElementException();

			Item item = current.item;
			current = current.next;
			return item;
		}
	}

	private void validateInput(Item item) {
		if (item == null)
			throw new java.lang.IllegalArgumentException();
	}

	public static void main(String[] args) {
		// unit testing
		Deque<Integer> intQ = new Deque<Integer>();
		intQ.addFirst(1);
		intQ.addLast(2);
		// intQ.removeFirst();
		// intQ.removeLast();
		// intQ.addFirst(3);
		for (Integer i : intQ)
			StdOut.println(i);
	}
}
