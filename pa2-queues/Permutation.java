/*
 * Write a client program Subset.java that takes a command-line integer k; reads in a sequence of N strings from standard input
 * using StdIn.readString(); and prints out exactly k of them, uniformly at random.
 * Each item from the sequence can be printed out at most once.
 * You may assume that 0 <= k <= N, where N is the number of string on standard input.
 *
 * The running time of Subset must be linear in the size of the input.
 * You may use only a constant amount of memory plus either one Deque or RandomizedQueue object of maximum size at most N,
 * where N is the number of strings on standard input.
 * (For an extra challenge, use only one Deque or RandomizedQueue object of maximum size at most k.)
 * It should have the following API.
 */
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class Permutation {
	public static void main(String[] args) {
		int k = Integer.parseInt(args[0]);
		int i = 0;
		int j = 0;
		RandomizedQueue<String> rqString = new RandomizedQueue<String>();
		while (!StdIn.isEmpty() && (k > 0)) {
			String s = StdIn.readString();
			if (i < k) {
				rqString.enqueue(s);
			} else {
				j = StdRandom.uniform(i + 1);
				if (j < k) {
					rqString.dequeue();
					rqString.enqueue(s);
				}
			}
			i++;
		}
		if (k < 0 || k > rqString.size())
			throw new java.lang.IllegalArgumentException();
		for (i = 0; i < k; i++)
			StdOut.println(rqString.dequeue());
	}
}