/*
 *  A randomized queue is similar to a stack or queue, except that the item removed is chosen uniformly
 *  at random from items in the data structure.
 *
 *  Performance requirements.
 *  Your randomized queue implementation must support each randomized queue operation (besides creating an iterator)
 *  in constant amortized time and use space proportional to the number of items currently in the queue.
 *  That is, any sequence of M randomized queue operations (starting from an empty queue) should take at most cM steps
 *  in the worst case, for some constant c.
 *  Additionally, your iterator implementation must support operations next() and hasNext() in constant worst-case time;
 *  and construction in linear time; you may use a linear amount of extra memory per iterator.
 */

import java.util.Iterator;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdOut;

public class RandomizedQueue<Item> implements Iterable<Item> {
	private Item[] node;
	private int N = 0;

	private void resize(int capacity) {
		Item[] copy = (Item[]) new Object[capacity];
		for (int i = 0; i < N; i++)
			copy[i] = node[i];
		node = copy;
	}

	public RandomizedQueue() {
		// construct an empty randomized queue
		node = (Item[]) new Object[1];
	}

	public boolean isEmpty() {
		// is the queue empty?
		return N == 0;
	}

	public int size() {
		// return the number of items on the queue
		return N;
	}

	public void enqueue(Item item) {
		// add the item
		if (item == null)
			throw new java.lang.IllegalArgumentException();

		if (N == node.length)
			resize(2 * node.length);
		node[N++] = item;
	}

	public Item dequeue() {
		// remove and return a random item
		if (isEmpty())
			throw new java.util.NoSuchElementException();

		int pos = StdRandom.uniform(N);
		Item item = node[pos];
		if (pos != N - 1)
			node[pos] = node[N - 1];
		node[N - 1] = null;
		N--;

		if (N > 0 && N == node.length / 4)
			resize(node.length / 2);

		return item;
	}

	public Item sample() {
		// return (but do not remove) a random item
		if (isEmpty())
			throw new java.util.NoSuchElementException();

		int pos = StdRandom.uniform(N);
		Item item = node[pos];
		return item;
	}

	@Override
	public Iterator<Item> iterator() {
		// return an independent iterator over items in random order
		return new RandomizedQueueIterator();
	}

	private class RandomizedQueueIterator implements Iterator<Item> {
		private Item[] copy;
		private int pos;

		public RandomizedQueueIterator() {
			// TODO Auto-generated constructor stub
			copy = (Item[]) new Object[N];
			for (int i = 0; i < N; i++) {
				if (node[i] != null) {
					copy[pos++] = node[i];
				}
			}
			pos -= 1;
			StdRandom.shuffle(copy);
		}

		@Override
		public boolean hasNext() {
			return pos != -1;
		}

		@Override
		public void remove() {
			throw new java.lang.UnsupportedOperationException();
		}

		@Override
		public Item next() {
			if (pos < 0)
				throw new java.util.NoSuchElementException();

			Item item = copy[pos--];
			return item;
		}
	}

	public static void main(String[] args) {
		// unit testing
		RandomizedQueue<Integer> rqInt = new RandomizedQueue<Integer>();
		rqInt.isEmpty();
		rqInt.enqueue(0);
		rqInt.enqueue(1);
		rqInt.enqueue(2);
		rqInt.enqueue(3);
		rqInt.enqueue(4);
		StdOut.println(rqInt.dequeue());
		for (Integer i : rqInt)
			StdOut.println("-" + i);
	}
}