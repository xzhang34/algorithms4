/*
 * What are the goals of this assignment?
 * 1. Set up a Java programming environment.
 * 2. Use our input and output libraries.
 * 3. Learn about a scientific application of the union-find data structure.
 * 4. Measure the running time of a program and use the doubling hypothesis to make predictions.
 * 5. Measure the amount of memory used by a data structure.
 */
import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {
	private final WeightedQuickUnionUF wUF1; // only with virtual top node
	private final WeightedQuickUnionUF wUF2; // with both virtual top and bottom node
	private boolean[] isOpen;
	private int numberOfOpensites;
	private final int size;

	// 4 adjacent delta distance, direction is left, top, right, bottom,
	private final int[][] dir = {
			{-1, 0},
			{0, -1},
			{1, 0},
			{0, 1}
	};

	public Percolation(int N) {
		// create N-by-N grid, with all sites blocked
		// node 0 is for open state, node N * N + 1 is virtual top node, N * N
		// +2 is virtual bottom node
		if (N <= 0)
			throw new java.lang.IllegalArgumentException();

		wUF1 = new WeightedQuickUnionUF(N * N + 2);
		wUF2 = new WeightedQuickUnionUF(N * N + 3);
		this.size = N;
		isOpen = new boolean[N * N + 1];
	}

	public void open(int i, int j) {
		// open site (row i, column j) if it is not open already
		// mark new site as open; connected it to all of its adjacent open
		// sites. (up to 4 calls to union)

		if (isOpen(i, j))
			return;

		isOpen[xyTo1D(i, j)] = true;
		numberOfOpensites++;

		if (i == 1) {
			wUF1.union(xyTo1D(i, j), size * size + 1);
			wUF2.union(xyTo1D(i, j), size * size + 1);
		}
		if (i == size) {
			wUF2.union(xyTo1D(i, j), size * size + 2);
		}
		for (int pos = 0; pos < dir.length; pos++) {
			int x = i + dir[pos][0];
			int y = j + dir[pos][1];
			if (x >= 1 && x <= size && y >= 1 && y <= size && isOpen(x, y)) {
				wUF1.union(xyTo1D(i, j), xyTo1D(x, y));
				wUF2.union(xyTo1D(i, j), xyTo1D(x, y));
			}
		}
	}

	public boolean isOpen(int i, int j) // is site (row i, column j) open?
	{
		validateInput(i, j);
		return isOpen[xyTo1D(i, j)];
	}

	public boolean isFull(int i, int j) // is site (row i, column j) full?
	{
		// is an open site that can be connected to an open site in the top row
		// via a chain
		// of neighboring (left, right, up, down) open sites
		validateInput(i, j);

		return wUF1.connected(xyTo1D(i, j), size * size + 1);
	}

	public int numberOfOpenSites()       // number of open sites
	{
		return numberOfOpensites;
	}

	public boolean percolates() // does the system percolate?
	{
		// system percolates if there is a full site in the bottom row
		// iff top and bottom are connected by open sites
		return wUF2.connected(size * size + 2, size * size + 1);
	}

	private int xyTo1D(int i, int j) {
		return (i - 1) * size + j;
	}

	private void validateInput(int i, int j) {
		if (i <= 0 || j <= 0 || i > size || j > size)
			throw new IllegalArgumentException("index i or j illeagle");
	}

	public static void main(String[] args) // test client (optional)
	{
		System.out.println("this is a test client and empty");
	}
}
