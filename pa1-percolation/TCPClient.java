import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;

public class TCPClient {
	public static void main(String argv[]) throws Exception {
		String msg;
		String response;
		BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
		Socket client = new Socket("paris.cs.utexas.edu", 35604);
		DataOutputStream outToServer = new DataOutputStream(client.getOutputStream());
		BufferedReader inFromServer = new BufferedReader(new InputStreamReader(client.getInputStream()));

		msg = inFromUser.readLine();
		outToServer.writeBytes(msg + '\n');
		response = inFromServer.readLine();
		System.out.println("from server: " + response);
		client.close();
	}

}
