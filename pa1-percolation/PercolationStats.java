import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;
import edu.princeton.cs.algs4.StdOut;

public class PercolationStats {
	private final int T;
	private final double[] Xt;
	private final static double CONFIDENCE_95 = 1.96;
	private double mean, stddev;

	public PercolationStats(int N, int T) {
		// perform T independent experiments on an N-by-N grid
		// StdOut.println("N/T is " + N + "/" + T);
		Percolation p;
		if (N <= 0 || T <= 0)
			throw new java.lang.IllegalArgumentException();
		this.T = T;
		Xt = new double[T];
		for (int t = 0; t < T; t++) {
			p = new Percolation(N);
			while (!p.percolates()) {
				int x = StdRandom.uniform(N) + 1;
				int y = StdRandom.uniform(N) + 1;
				// StdOut.println(x + ", " + y);
				if (p.isOpen(x, y))
					continue;
				p.open(x, y);
			}
			// StdOut.println(t + ": is " + p.numberOfOpenSites());
			Xt[t] = (double)p.numberOfOpenSites() / (N * N);
		}
		mean =  StdStats.mean(Xt);
		stddev = StdStats.stddev(Xt);
	}

	public double mean() {
		// sample mean of percolation threshold
		return mean;

	}

	public double stddev() {
		// sample standard deviation of percolation threshold
		return stddev;

	}

	public double confidenceLo() {
		// low endpoint of 95% confidence interval
		return mean - CONFIDENCE_95 * stddev / Math.sqrt(T);

	}

	public double confidenceHi() {
		// high endpoint of 95% confidence interval
		return mean + CONFIDENCE_95 * stddev / Math.sqrt(T);
	}

	public static void main(String[] args) {
		// test client (described below)
		if (args.length != 2)
			throw new java.lang.IllegalArgumentException();

		PercolationStats p = new PercolationStats(Integer.parseInt(args[0]),
				Integer.parseInt(args[1]));
		StdOut.println("mean \t = " + p.mean());
		StdOut.println("stddev \t = " + p.stddev());
		StdOut.println("95% confidence interval = " + p.confidenceLo() + ", "
				+ p.confidenceHi());

	}
}
