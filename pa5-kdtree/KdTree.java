/*
 * A 2d-tree is a generalization of a BST to two-dimensional keys.
 * The idea is to build a BST with points in the nodes, using the x- and y-coordinates
 * of the points as keys in strictly alternating sequence.
 *
 * Search and insert. The algorithms for search and insert are similar to those for BSTs,
 * but at the root we use the x-coordinate (if the point to be inserted has a smaller
 * x-coordinate than the point at the root, go left; otherwise go right); 
 * then at the next level, we use the y-coordinate (if the point to be inserted has a
 * smaller y-coordinate than the point in the node, go left; otherwise go right);
 * then at the next level the x-coordinate, and so forth.
 *
 * Draw. A 2d-tree divides the unit square in a simple way: all the points to the left of
 * the root go in the left subtree; all those to the right go in the right subtree; 
 * and so forth, recursively. Your draw() method should draw all of the points to standard
 * draw in black and the subdivisions in red (for vertical splits) and blue
 * (for horizontal splits). This method need not be efficient and it is primarily for debugging.
 *
 * The prime advantage of a 2d-tree over a BST is that it supports efficient implementation
 * of range search and nearest neighbor search. 
 * Each node corresponds to an axis-aligned rectangle in the unit square, which encloses
 * all of the points in its subtree. The root corresponds to the unit square;
 * the left and right children of the root corresponds to the two rectangles
 * split by the x-coordinate of the point at the root; and so forth.
 *
 * Range search. To find all points contained in a given query rectangle, start at the root and recursively
 * search for points in both subtrees using the following pruning rule: if the query rectangle does not
 * intersect the rectangle corresponding to a node, there is no need to explore that node (or its subtrees).
 * A subtree is searched only if it might contain a point contained in the query rectangle.
 *
 * Nearest neighbor search. To find a closest point to a given query point, start at the root and recursively
 * search in both subtrees using the following pruning rule: if the closest point discovered so far is closer
 * than the distance between the query point and the rectangle corresponding to a node, there is no need to
 * explore that node (or its subtrees). That is, a node is searched only if it might contain a point that
 * is closer than the best one found so far. The effectiveness of the pruning rule depends on quickly
 * finding a nearby point. To do this, organize your recursive method so that when there are two
 * possible subtrees to go down, you always choose the subtree that is on the same side of the
 * splitting line as the query point as the first subtree to explore—the closest point found
 * while exploring the first subtree may enable pruning of the second subtree.
 */

import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.RedBlackBST;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdDraw;

public class KdTree {
	private Node root;
	private int size;
	private final int HORIZONTAL = 0;
	private final int VERTICAL = 1;
	private final int LEFT = 0; // small one
	private final int RIGHT = 1; // big one
	private Point2D min = null;
	private double dist = Double.POSITIVE_INFINITY;

	private class Node {
		private Point2D key; // the point
		private Integer val; // the symbol table maps the point to this value
		private RectHV rect; // the axis-aligned rectangle with this node
		private int ori;
		private Node left; // the left/bottom subtree
		private Node right; // the right/top subtree

		public Node(Point2D key, Integer val, RectHV rect, int ori) {
			this.key = key;
			this.val = val;
			this.rect = rect;
			this.ori = ori;
		}
	}

	/***********************************************************************
	 * Search BST for given key, and return associated value if found, return
	 * null if not found
	 ***********************************************************************/
	// return value associated with the given key, or null if no such key exists
	private Integer get(Point2D key) {
		return get(root, key);
	}

	private Integer get(Node x, Point2D key) {
		if (x == null)
			return null;

		// StdOut.println("current point " + x.key);
		if (key.equals(x.key))
			return x.val;

		double cmp = 0;
		if (x.ori == VERTICAL) {
			cmp = key.x() - x.key.x();
			// StdOut.println("x axis point difference is " + cmp);
		} else {
			cmp = key.y() - x.key.y();
			// StdOut.println("y axis point difference is " + cmp);
		}

		if (cmp < 0) {
			// StdOut.println("case left");
			return get(x.left, key);
		} else if (cmp > 0) {
			// StdOut.println("case right");
			return get(x.right, key);
		} else {
			// StdOut.println("case bingo: " + key + " with compare node " +
			// x.key);
			if (key.equals(x.key)) {
				return x.val;
			} else
				return get(x.right, key);
		}
	}

	/***********************************************************************
	 * Insert key-value pair into BST If key already exists, update with new
	 * value
	 ***********************************************************************/
	private void put(Point2D key, Integer val) {
		root = put(root, root, key, val, -1);
	}

	private Node put(Node x, Node p, Point2D key, Integer val, int dir) {
		if (x == null) { // put new node
			RectHV rect;
			int ori;
			if (p == null) {
				// rect = new RectHV(Double.NEGATIVE_INFINITY,
				// Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY,
				// Double.POSITIVE_INFINITY);
				// StdOut.print("case root node: ");
				rect = new RectHV(0, 0, 1, 1);
				ori = VERTICAL;
			} else {
				// StdOut.print("case p node: " + p.key);
				if (p.ori == VERTICAL) {
					ori = HORIZONTAL;
					if (dir == LEFT) {// smaller
						rect = new RectHV(p.rect.xmin(), p.rect.ymin(),
								p.key.x(), p.rect.ymax());
						// StdOut.print("case 1: ");
					} else {
						// StdOut.print("case 2: ");
						rect = new RectHV(p.key.x(), p.rect.ymin(),
								p.rect.xmax(), p.rect.ymax());
					}
				} else {
					ori = VERTICAL;
					if (dir == LEFT) {// bottom
						// StdOut.print("case 3: ");
						rect = new RectHV(p.rect.xmin(), p.rect.ymin(),
								p.rect.xmax(), p.key.y());
					} else {
						// StdOut.print("case 4: ");
						rect = new RectHV(p.rect.xmin(), p.key.y(),
								p.rect.xmax(), p.rect.ymax());
					}
				}
			}
			// StdOut.println("put node: " + key + " with rect is " + rect
			// + " ori is "
			// + (ori == VERTICAL ? " VERTICAL " : "HORIZONTAL"));
			return new Node(key, val, rect, ori);
		}

		double cmp = 0;
		if (x.ori == VERTICAL) {
			cmp = key.x() - x.key.x();
			// StdOut.println("x axis point difference is " + cmp);
		} else {
			cmp = key.y() - x.key.y();
			// StdOut.println("y axis point difference is " + cmp);
		}

		if (cmp < 0) {
			// StdOut.println("case left");
			x.left = put(x.left, x, key, val, LEFT);
		} else if (cmp > 0) {
			// StdOut.println("case right");
			x.right = put(x.right, x, key, val, RIGHT);
		} else {
			// StdOut.println("case bingo: " + key + " with compare node " +
			// x.key);
			if (key.equals(x.key)) {
				x.val = val;
				size--;
			} else
				x.right = put(x.right, x, key, val, RIGHT);
		}
		return x;
	}

	/***********************************************************************
	 * Range count and range search.
	 ***********************************************************************/
	private Iterable<Point2D> keys() {
		Queue<Point2D> queue = new Queue<Point2D>();
		keys(root, queue);
		return queue;
	}

	private void keys(Node x, Queue<Point2D> queue) {
		if (x == null)
			return;
		keys(x.left, queue);
		queue.enqueue(x.key);
		keys(x.right, queue);
	}

	// main API
	public KdTree() {
		// construct an empty set of points
	}

	public boolean isEmpty() {
		// is the set empty?
		return size == 0;
	}

	public int size() {
		// number of points in the set
		return size;
	}

	public void insert(Point2D p) {
		// add the point to the set (if it is not already in the set)
		if (p == null)
			throw new java.lang.IllegalArgumentException();

		if (contains(p))
			return;

		size++;
		put(p, 1);
	}

	public boolean contains(Point2D p) {
		// does the set contain point p?
		if (p == null)
			throw new java.lang.IllegalArgumentException();

		return get(p) != null;
	}

	// red color for vertical line, blue color for horizontal line
	private void draw(Node x, Node p) {
		if (x == null)
			return;

		StdDraw.setPenColor(StdDraw.BLACK);
		StdDraw.setPenRadius(.01);
		x.key.draw();
		StdDraw.textLeft(x.key.x(), x.key.y(), x.key.toString());
		// x.rect.draw();
		if (x.ori == VERTICAL) {
			StdDraw.setPenColor(StdDraw.RED);
			StdDraw.setPenRadius();
			StdDraw.line(x.key.x(), x.rect.ymin(), x.key.x(), x.rect.ymax());
		} else {
			StdDraw.setPenColor(StdDraw.BLUE);
			StdDraw.setPenRadius();
			if (x.key.x() > p.key.x())
				StdDraw.line(x.rect.xmin(), x.key.y(), x.rect.xmax(), x.key.y());
			else
				StdDraw.line(x.rect.xmin(), x.key.y(), p.key.x(), x.key.y());
		}

		draw(x.left, x);
		draw(x.right, x);
	}

	public void draw() {
		// draw all points to standard draw
		draw(root, null);
	}

	/*
	 * Range search. To find all points contained in a given query rectangle,
	 * start at the root and recursively search for points in both subtrees
	 * using the following pruning rule: if the query rectangle does not
	 * intersect the rectangle corresponding to a node, there is no need to
	 * explore that node (or its subtrees). A subtree is searched only if it
	 * might contain a point contained in the query rectangle.
	 */
	private void range(Node x, RectHV rect, Queue<Point2D> queue) {
		if (x == null)
			return;
		if (!rect.intersects(x.rect))
			return;

		if (rect.contains(x.key))
			queue.enqueue(x.key);
		range(x.left, rect, queue);
		range(x.right, rect, queue);
	}

	public Iterable<Point2D> range(RectHV rect) {
		// all points that are inside the rectangle
		if (rect == null)
			throw new java.lang.IllegalArgumentException();

		Queue<Point2D> queue = new Queue<Point2D>();
		range(root, rect, queue);
		return queue;
	}

	/*
	 * Nearest neighbor search. To find a closest point to a given query point,
	 * start at the root and recursively search in both subtrees using the
	 * following pruning rule: if the closest point discovered so far is closer
	 * than the distance between the query point and the rectangle corresponding
	 * to a node, there is no need to explore that node (or its subtrees). That
	 * is, a node is searched only if it might contain a point that is closer
	 * than the best one found so far. The effectiveness of the pruning rule
	 * depends on quickly finding a nearby point. To do this, organize your
	 * recursive method so that when there are two possible subtrees to go down,
	 * you always choose the subtree that is on the same side of the splitting
	 * line as the query point as the first subtree to explore—the closest point
	 * found while exploring the first subtree may enable pruning of the second
	 * subtree.
	 */
	private void nearest(Node x, Point2D p) {
		if (x == null)
			return;

		if (dist < x.rect.distanceSquaredTo(p))
			return;

		if (x.key.distanceSquaredTo(p) < dist) {
			dist = x.key.distanceSquaredTo(p);
			min = x.key;
		}

		if (x.ori == VERTICAL) { // compare x axis
			if (p.x() < x.key.x()) {
				nearest(x.left, p);
				nearest(x.right, p);
			} else {
				nearest(x.right, p);
				nearest(x.left, p);
			}
		} else { // compare y axis
			if (p.y() < x.key.y()) {
				nearest(x.left, p);
				nearest(x.right, p);
			} else {
				nearest(x.right, p);
				nearest(x.left, p);
			}
		}
	}

	public Point2D nearest(Point2D p) {
		// a nearest neighbor in the set to point p; null if the set is empty
		if (p == null)
			throw new java.lang.IllegalArgumentException();

		dist = Double.POSITIVE_INFINITY;
		min = null;
		nearest(root, p);
		return min;
	}

	public static void main(String[] args) {
		// unit testing of the methods (optional)
		Point2D p0 = new Point2D(2, 3);
		Point2D p1 = new Point2D(4, 2);
		Point2D p2 = new Point2D(4, 5);
		Point2D p3 = new Point2D(3, 3);
		Point2D p4 = new Point2D(1, 5);
		Point2D p5 = new Point2D(4, 4);
		Point2D p6 = new Point2D(8, 3);
		RectHV rect = new RectHV(0.4, 0.3, 0.6, 0.5);

		KdTree set = new KdTree();
		set.insert(p0);
		set.insert(p1);
		set.insert(p2);
		set.insert(p3);
		set.insert(p4);
		set.insert(p5);
		StdOut.println(set.size());
		for (Point2D x : set.keys())
			StdOut.println(x);

		StdOut.println("===");
		StdOut.println(p6 + " is contains " + set.contains(p6));
	}
}
