/*
 * Write a mutable data type PointSET.java that represents a set of points in the unit square.
 * Implement the following API by using a red-black BST
 * (using either SET from algs4.jar or java.util.TreeSet).
 * Corner cases. Throw a java.lang.IllegalArgumentException if any argument is null.
 * Performance requirements. Your implementation should support insert() and contains() in time
 * proportional to the logarithm of the number of points in the set in the worst case;
 * it should support nearest() and range() in time proportional to the number of points
 * in the set.
 */
import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.RedBlackBST;
import edu.princeton.cs.algs4.StdOut;

public class PointSET {
	private RedBlackBST<Point2D, Integer> rbtree;

	public PointSET() {
		// construct an empty set of points
		rbtree = new RedBlackBST<Point2D, Integer>();
	}

	public boolean isEmpty() {
		// is the set empty?
		return rbtree.isEmpty();
	}

	public int size() {
		// number of points in the set
		return rbtree.size();
	}

	public void insert(Point2D p) {
		// add the point to the set (if it is not already in the set)
		if (p == null)
			throw new java.lang.IllegalArgumentException();

		if (!rbtree.contains(p))
			rbtree.put(p, 1);
	}

	public boolean contains(Point2D p) {
		// does the set contain point p?
		if (p == null)
			throw new java.lang.IllegalArgumentException();

		return rbtree.contains(p);
	}

	public void draw() {
		// draw all points to standard draw
		for (Point2D p : rbtree.keys())
			p.draw();
	}

	public Iterable<Point2D> range(RectHV rect) {
		// all points that are inside the rectangle
		if (rect == null)
			throw new java.lang.IllegalArgumentException();

		// Point2D lo = new Point2D(rect.xmin(), rect.ymin());
		// Point2D hi = new Point2D(rect.xmax(), rect.ymax());
		// StdOut.println("lo is " + lo + " hi is " + hi);
		// return rbtree.keys(lo, hi);
		Queue<Point2D> range = new Queue<Point2D>();
		for (Point2D p : rbtree.keys())
			if (rect.contains(p))
				range.enqueue(p);
		return range;
	}

	public Point2D nearest(Point2D p) {
		// a nearest neighbor in the set to point p; null if the set is empty
		if (p == null)
			throw new java.lang.IllegalArgumentException();

		double mindist = Double.POSITIVE_INFINITY;
		Point2D minP = null;
		for (Point2D key : rbtree.keys()) {
			if (key.distanceTo(p) < mindist) {
				mindist = key.distanceTo(p);
				minP = key;
			}
		}
		return minP;
	}

	public static void main(String[] args) {
		// unit testing of the methods (optional)
		// StdDraw.show(0);
		Point2D p0 = new Point2D(0.0, 0.0);
		Point2D p1 = new Point2D(0.1, 0.4);
		Point2D p2 = new Point2D(0.6, 0.5);
		Point2D p = new Point2D(0.4, 0.3);
		RectHV rect = new RectHV(0.4, 0.3, 0.6, 0.5);

		PointSET set = new PointSET();
		set.insert(p0);
		set.insert(p1);
		set.insert(p2);

		for (Point2D x : set.range(rect))
			StdOut.println(x);

		StdOut.println("nearest p is " + set.nearest(p));

	}
}
