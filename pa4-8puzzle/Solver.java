/*
 * The 8-puzzle problem is a puzzle invented and popularized by Noyes Palmer Chapman in the 1870s.
 * It is played on a 3-by-3 grid with 8 square blocks labeled 1 through 8 and a blank square.
 * Your goal is to rearrange the blocks so that they are in order, using as few moves as possible.
 * You are permitted to slide blocks horizontally or vertically into the blank square.
 */

/*
 * Best-first search.
 * Now, we describe a solution to the problem that illustrates a general artificial intelligence
 * methodology known as the A* search algorithm. We define a search node of the game to be a board,
 * the number of moves made to reach the board, and the previous search node.
 * First, insert the initial search node (the initial board, 0 moves, and a null previous search node)
 * into a priority queue.
 * Then, delete from the priority queue the search node with the minimum priority,
 * and insert onto the priority queue all neighboring search nodes
 * (those that can be reached in one move from the dequeued search node).
 * Repeat this procedure until the search node dequeued corresponds to a goal board.
 *
 * The success of this approach hinges on the choice of priority function for a search node.
 * 1. Hamming priority function. The number of blocks in the wrong position, plus the number of moves
 * made so far to get to the search node. Intuitively, a search node with a small number of blocks
 * in the wrong position is close to the goal, and we prefer a search node that have been reached
 * using a small number of moves.
 * 2. Manhattan priority function. The sum of the Manhattan distances (sum of the vertical and horizontal distance)
 * from the blocks to their goal positions, plus the number of moves made so far to get to the search node.
 */

/*
 * A critical optimization. Best-first search has one annoying feature: search nodes corresponding to the same board
 * are enqueued on the priority queue many times.
 * To reduce unnecessary exploration of useless search nodes, when considering the neighbors of a search node,
 * don't enqueue a neighbor if its board is the same as the board of the predecessor search node.
 *
 * A second optimization. To avoid recomputing the Manhattan priority of a search node from scratch each time
 * during various priority queue operations, pre-compute its value when you construct the search node;
 * save it in an instance variable; and return the saved value as needed. This caching technique is broadly applicable:
 * consider using it in any situation where you are recomputing the same quantity many times and
 * for which computing that quantity is a bottleneck operation.
 */

import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.MinPQ;
public class Solver {
	private int moves;
	private Stack<Board> result = new Stack<Board>();
	private Stack<Board> resultTwin = new Stack<Board>();
	private boolean resolved = false;

	private class searchNode implements Comparable<searchNode> {
		private Board cur;
		private int moves = 0;
		private searchNode prev;
		private int priority = 0;

		public searchNode(Board b, int m, searchNode p) {
			cur = b;
			moves = m;
			prev = p;
			priority = m + cur.manhattan();
		}

		@Override
		public int compareTo(searchNode that) {
			return this.priority - that.priority;
		}
	}

	public Solver(Board initial) {
		// find a solution to the initial board (using the A* algorithm)
		if (initial == null)
			throw new java.lang.IllegalArgumentException("null argument");

		MinPQ<searchNode> pq = new MinPQ<searchNode>();
		MinPQ<searchNode> pqTwin = new MinPQ<searchNode>();

		pq.insert(new searchNode(initial, moves, null));
		pqTwin.insert(new searchNode(initial.twin(), moves, null));

		while (!pq.isEmpty() && !pqTwin.isEmpty()) {
			searchNode node = pq.delMin();
			if (node.cur.isGoal()) {
				moves = node.moves;
				result.push(node.cur);
				while (node.prev != null) {
					result.push(node.prev.cur);
					node = node.prev;
				}
				resolved = true;
				return;
			}

			searchNode nodeTwin = pqTwin.delMin();
			if (nodeTwin.cur.isGoal()) {
				moves = nodeTwin.moves;
				resultTwin.push(nodeTwin.cur);
				while (nodeTwin.prev != null) {
					resultTwin.push(nodeTwin.prev.cur);
					nodeTwin = nodeTwin.prev;
				}
				resolved = false;
				return;
			}

			for (Board nb : node.cur.neighbors()) {
				if (node.prev != null && nb.equals(node.prev.cur))
					continue;
				pq.insert(new searchNode(nb, node.moves + 1, node));
			}

			for (Board nb : nodeTwin.cur.neighbors()) {
				if (nodeTwin.prev != null && nb.equals(nodeTwin.prev.cur))
					continue;
				pqTwin.insert(new searchNode(nb, nodeTwin.moves + 1, nodeTwin));
			}
		}
	}

	public boolean isSolvable() {
		// is the initial board solvable?
		return resolved;
	}

	public int moves() {
		// min number of moves to solve initial board; -1 if unsolvable
		if (resolved)
			return moves;
		else
			return -1;
	}

	public Iterable<Board> solution() {
		// sequence of boards in a shortest solution; null if unsolvable
		if (resolved)
			return result;
		else
			return null;
	}

	public static void main(String[] args) {
		// create initial board from file
		In in = new In(args[0]);
		int N = in.readInt();
		int[][] blocks = new int[N][N];
		for (int i = 0; i < N; i++)
			for (int j = 0; j < N; j++)
				blocks[i][j] = in.readInt();
		Board initial = new Board(blocks);

		// solve the puzzle
		Solver solver = new Solver(initial);

		// print solution to standard output
		if (!solver.isSolvable())
			StdOut.println("No solution possible");
		else {
			StdOut.println("Minimum number of moves = " + solver.moves());
			for (Board board : solver.solution())
				StdOut.println(board);
		}
	}
}
