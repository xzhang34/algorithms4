import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;
/*
 * The 8-puzzle problem is a puzzle invented and popularized by Noyes Palmer Chapman in the 1870s.
 * It is played on a 3-by-3 grid with 8 square blocks labeled 1 through 8 and a blank square.
 * Your goal is to rearrange the blocks so that they are in order, using as few moves as possible.
 * You are permitted to slide blocks horizontally or vertically into the blank square.
 */
public class Board {
	private int node[];
	private int N;

	/*
	 * The input and output format for a board is the board dimension N followed
	 * by the N-by-N initial board, using 0 to represent the blank square.
	 */
	public Board(int[][] blocks) {
		// construct a board from an N-by-N array of blocks
		// (where blocks[i][j] = block in row i, column j)
		if (blocks == null)
			throw new IllegalArgumentException("null argument");
		N = blocks.length;
		node = new int[N * N];
		for (int i = 0; i < N; i++)
			for (int j = 0; j < N; j++)
				node[i * N + j] = blocks[i][j];
	}

	public int dimension() {
		// board dimension N
		return N;
	}

	public int hamming() {
		// number of blocks out of place
		// number of blocks in the wrong position, plus the number of moves made
		// so far to get to the search node
		int count = 0;
		for (int i = 0; i < N * N - 1; i++)
			if (node[i] != i + 1)
				count++;
		return count;
	}

	public int manhattan() {
		// sum of Manhattan distances between blocks and goal
		// The sum of the Manhattan distances (sum of the vertical and
		// horizontal distance) from the blocks to their goal positions, plus
		// the number of moves made so far to get to the search node.
		int count = 0;
		for (int k = 1; k < N * N; k++) {
			for (int index = 0; index < N * N; index++)
				if (node[index] == k) {
					int cur_i = index / N;
					int cur_j = index % N;
					int goal_i = (k - 1) / N;
					int goal_j = (k - 1) % N;

					count += Math.abs(cur_i - goal_i)
							+ Math.abs(cur_j - goal_j);
				}
		}
		return count;
	}

	public boolean isGoal() {
		// is this board the goal board?
		for (int i = 0; i < N * N - 1; i++)
			if (node[i] != i + 1)
				return false;
		if (node[N * N - 1] != 0)
			return false;
		return true;
	}

	public Board twin() {
		// a board that is obtained by exchanging two adjacent blocks in the
		// same row
		int blocks[][] = new int[N][N];
		for (int i = 0; i < N; i++)
			for (int j = 0; j < N; j++)
				blocks[i][j] = node[i * N + j];
		int temp = 0;
		if (blocks[0][0] != 0) {
			if (blocks[0][1] != 0) {
				temp = blocks[0][0];
				blocks[0][0] = blocks[0][1];
				blocks[0][1] = temp;
			} else {
				temp = blocks[1][0];
				blocks[1][0] = blocks[1][1];
				blocks[1][1] = temp;
			}
		} else {
			temp = blocks[1][0];
			blocks[1][0] = blocks[1][1];
			blocks[1][1] = temp;
		}
		return new Board(blocks);
	}

	@Override
	public boolean equals(Object y) {
		// does this board equal y?
		if (this == y)
			return true;
		if (y == null)
			return false;
		if (this.getClass() != y.getClass())
			return false;
		Board that = (Board) y;
		if (this.dimension() != that.dimension())
			return false;
		for (int i = 0; i < N * N; i++)
			if (this.node[i] != that.node[i])
				return false;
		return true;
	}

	public Iterable<Board> neighbors() {
		// all neighboring boards
		// Add the items you want to a Stack<Board> or Queue<Board> and return
		// that.
		Queue<Board> neighborQ = new Queue<Board>();
		// neighbors: left, top, right, bottom
		int dx[] = { -1, 0, 1, 0 };
		int dy[] = { 0, -1, 0, 1 };
		int blank_pos = 0;
		for (blank_pos = 0; blank_pos < N * N; blank_pos++)
			if (node[blank_pos] == 0)
				break;
		int blank_i = blank_pos / N;
		int blank_j = blank_pos % N;
		for (int dir = 0; dir < 4; dir++) {
			int x = blank_i + dx[dir];
			int y = blank_j + dy[dir];
			if (x >= 0 && x < N && y >= 0 && y < N) {
				int blocks[][] = new int[N][N];
				for (int i = 0; i < N; i++)
					for (int j = 0; j < N; j++)
						blocks[i][j] = node[i * N + j];
				// swap blank & item
				blocks[blank_i][blank_j] = blocks[x][y];
				blocks[x][y] = 0;
				neighborQ.enqueue(new Board(blocks));
			}

		}
		return neighborQ;
	}

	@Override
	public String toString() {
		// string representation of this board (in the output format specified
		// below)
		StringBuilder s = new StringBuilder();
		s.append(N + "\n");
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				s.append(String.format("%2d ", node[i * N + j]));
			}
			s.append("\n");
		}
		return s.toString();
	}

	public static void main(String[] args) {
		// unit tests (not graded)
		// create initial board from file
		In in = new In(args[0]);
		int N = in.readInt();
		int[][] blocks = new int[N][N];
		for (int i = 0; i < N; i++)
			for (int j = 0; j < N; j++)
				blocks[i][j] = in.readInt();
		Board board = new Board(blocks);
		StdOut.println(board + "ham is " + board.hamming() + ", manhattan is "
				+ board.manhattan());

		StdOut.println("twin s:  ");
		StdOut.println(board.twin() + "is goal " + board.isGoal()
				+ " is equal " + (board == board.twin()));
		StdOut.println("Neghbors are:  ");
		for (Board nb : board.neighbors())
			StdOut.println(nb);

		int[][] blocks1 = { { 1, 0, 2 }, { 7, 5, 4 }, { 8, 6, 3 } };
		int[][] blocks2 = { { 1, 0, 2 }, { 7, 5, 4 }, { 8, 6, 3 } };
		Board b1 = new Board(blocks1);
		Board b2 = new Board(blocks2);
		StdOut.println("b1 == b2 ? " + (b1.equals(b2)));

	}
}
