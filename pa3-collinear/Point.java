/*************************************************************************
 * Name:
 * Email:
 *
 * Compilation:  javac Point.java
 * Execution:
 * Dependencies: StdDraw.java
 *
 * Description: An immutable data type for points in the plane.
 *
 *************************************************************************/

import java.util.Comparator;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

public class Point implements Comparable<Point> {

	private final int x; // x coordinate
	private final int y; // y coordinate

    /**
     * Initializes a new point.
     *
     * @param  x the <em>x</em>-coordinate of the point
     * @param  y the <em>y</em>-coordinate of the point
     */
	public Point(int x, int y) {
		/* DO NOT MODIFY */
		this.x = x;
		this.y = y;
	}

    /**
     * Draws this point to standard draw.
     */
	public void draw() {
		/* DO NOT MODIFY */
		StdDraw.point(x, y);
	}

    /**
     * Draws the line segment between this point and the specified point
     * to standard draw.
     *
     * @param that the other point
     */
	public void drawTo(Point that) {
		/* DO NOT MODIFY */
		StdDraw.line(this.x, this.y, that.x, that.y);
	}

	private boolean compare2P(Point v, Point w) {
		if (v.x != w.x)
			return false;
		if (v.y != w.y)
			return false;
		return true;
	}

	private class BySlope implements Comparator<Point> {
		@Override
		public int compare(Point v, Point w) {
			double slope1 = slopeTo(v);
			double slope2 = slopeTo(w);
			// StdOut.println(slope1 + ", " + slope2 + Point.this + "v is " + v
			// + " w is " + w);
			if (compare2P(v, w)
					|| (slope1 == Double.POSITIVE_INFINITY && slope2 == Double.POSITIVE_INFINITY)
					|| (slope1 == Double.NEGATIVE_INFINITY && slope2 == Double.NEGATIVE_INFINITY))
				return 0;
			if (slope1 < slope2)
				return -1;
			if (slope1 > slope2)
				return 1;
			return 0;
		}
	}

    /**
     * Returns the slope between this point and the specified point.
     * Formally, if the two points are (x0, y0) and (x1, y1), then the slope
     * is (y1 - y0) / (x1 - x0). For completeness, the slope is defined to be
     * +0.0 if the line segment connecting the two points is horizontal;
     * Double.POSITIVE_INFINITY if the line segment is vertical;
     * and Double.NEGATIVE_INFINITY if (x0, y0) and (x1, y1) are equal.
     *
     * @param  that the other point
     * @return the slope between this point and the specified point
     */
	public double slopeTo(Point that) {
		/* YOUR CODE HERE */
		if ((that.x == this.x) && (that.y == this.y))
			return Double.NEGATIVE_INFINITY;
		if (that.x == this.x)
			return Double.POSITIVE_INFINITY;
		if (that.y == this.y)
			return 0;
		return (double) (that.y - this.y) / (double) (that.x - this.x);
	}

    /**
     * Compares two points by y-coordinate, breaking ties by x-coordinate.
     * Formally, the invoking point (x0, y0) is less than the argument point
     * (x1, y1) if and only if either y0 < y1 or if y0 = y1 and x0 < x1.
     *
     * @param  that the other point
     * @return the value <tt>0</tt> if this point is equal to the argument
     *         point (x0 = x1 and y0 = y1);
     *         a negative integer if this point is less than the argument
     *         point; and a positive integer if this point is greater than the
     *         argument point
     */
	public int compareTo(Point that) {
		/* YOUR CODE HERE */
		if (this.y < that.y)
			return -1;
		if (this.y > that.y)
			return 1;
		if (this.x < that.x)
			return -1;
		if (this.x > that.x)
			return 1;
		return 0;
	}

    /**
     * Compares two points by the slope they make with this point.
     * The slope is defined as in the slopeTo() method.
     *
     * @return the Comparator that defines this ordering on points
     */
	public Comparator<Point> slopeOrder() {
		// compare two points by slopes they make with this point
		return new BySlope();
	}
	// return string representation of this point
	@Override
	public String toString() {
		/* DO NOT MODIFY */
		return "(" + x + ", " + y + ")";
	}

	// unit test
	public static void main(String[] args) {
		/* YOUR CODE HERE */
		Point[] points = new Point[7];
		points[0] = new Point(19000, 10000);
		points[1] = new Point(18000, 10000);
		points[2] = new Point(32000, 10000);
		points[3] = new Point(21000, 10000);
		points[4] = new Point(1234, 5678);
		points[5] = new Point(14000, 10000);
		points[6] = new Point(0, 0);

		for (Point p : points) {
			StdOut.println(p + "slope is " + points[0].slopeTo(p));
		}

		Point p = new Point(400, 168);
		Point q = new Point(379, 155);
		Point r = new Point(421, 181);

		StdOut.println(p.slopeTo(q) + " - " + p.slopeTo(r));
		// StdOut.println(p.SLOPE_ORDER.compare(q, r) + ", "
		//		+ p.SLOPE_ORDER.compare(r, q));

		StdDraw.setXscale(0, 500);
		StdDraw.setYscale(0, 500);
		StdDraw.enableDoubleBuffering();
		StdDraw.show();
		StdDraw.setPenRadius(0.01); // make the points a bit larger
		p.draw();
		q.draw();
		r.draw();
		p.drawTo(q);
		// p.drawTo(r);
		// display to screen all at once
		StdDraw.enableDoubleBuffering();
		StdDraw.show();		// reset the pen radius
		StdDraw.setPenRadius();
	}
}
