import java.util.ArrayList;
import java.util.Arrays;

public class FastCollinearPoints {
	private ArrayList<LineSegment> ls = new ArrayList<LineSegment>();
	private int size;
	public FastCollinearPoints(Point[] ps) {
		if (ps == null)
			throw new IllegalArgumentException("input is null");
		int N = ps.length;
		Point[] points = new Point[N];
		for (int j = 0; j < N; j++) {
			if (ps[j] == null)
				throw new IllegalArgumentException("input is null");
			points[j] = ps[j];
		}
		Arrays.sort(points);

		for (int j = 0; j < N - 1; j++)
			if (points[j].compareTo(points[j+1]) == 0)
				throw new IllegalArgumentException("input is null");

		for (int i = 0; i < N; i++) {
			Point[] copy = new Point[N];
			for (int j = 0; j < N; j++)
				copy[j] = points[j];

			int key = i;
			Arrays.sort(copy, points[key].slopeOrder());
			// for (Point p : copy) {
			// StdOut.println(key + " :" + ps[key] + " - " + p + " slope is "
			// + ps[key].slopeTo(p));
			// }
			// StdOut.println("==");
			double lastSlope = Double.NaN;
			for (int start = 0; start < N;) {
				double slope = points[key].slopeTo(copy[start]);
				// StdOut.println(ps[key] + " with slope " + slope);
				int stop = start + 1;
				while (stop < N && slope == points[key].slopeTo(copy[stop]))
					stop++;

				if (stop - start >= 3 && lastSlope != slope) {
					lastSlope = slope;
					if (points[key].compareTo(copy[start]) > 0)
						continue;
					ls.add(new LineSegment(points[key], copy[stop - 1]));
					//StdOut.print(points[key] + " -> ");
					//for (int k = start; k < stop - 1; k++) {
						//StdOut.print(copy[k] + " -> ");
					//}
					//points[key].drawTo(copy[stop - 1]);
					//StdOut.println(copy[stop - 1]);
					// display to screen all at once
					//StdDraw.show(0);
					// reset the pen radius
					//StdDraw.setPenRadius();
				}
				start = stop;
			}
		}
		size = ls.size();
	}
	public int numberOfSegments() {
		// the number of line segments
		return size;
	}
	public LineSegment[] segments() {
		// the line segments
		LineSegment[] res = new LineSegment[size];
		int i = 0;
		for (LineSegment e: ls) {
			res[i++] = e;
		}
		return res;
	}
}