import java.util.ArrayList;
import java.util.Arrays;

public class BruteCollinearPoints{
	private ArrayList<LineSegment> ls = new ArrayList<LineSegment>();
	private int size;
	public BruteCollinearPoints(Point[] ps) {
		if (ps == null)
			throw new IllegalArgumentException("input is null");
		int N = ps.length;
		Point[] points = new Point[N];
		for (int j = 0; j < N; j++) {
			if (ps[j] == null)
				throw new IllegalArgumentException("input is null");
			points[j] = ps[j];
		}

		// finds all line segments containing 4 points
		Arrays.sort(points);

		for (int j = 0; j < N - 1; j++)
			if (points[j].compareTo(points[j+1]) == 0)
				throw new IllegalArgumentException("input is null");

		for (int p = 0; p < N; p++)
			for (int q = p + 1; q < N; q++)
				for (int r = q + 1; r < N; r++)
					for (int s = r + 1; s < N; s++) {
						if ((points[p].slopeTo(points[q]) == points[p].slopeTo(points[r]))
								&& (points[p].slopeTo(points[q]) == points[p]
										.slopeTo(points[s]))) {
							ls.add(new LineSegment(points[p], points[s]));
						}

					}
		size = ls.size();

	}

	public int numberOfSegments() {
		// the number of line segments
		return size;
	}

	public LineSegment[] segments() {
		// the line segments
		LineSegment[] res = new LineSegment[size];
		int i = 0;
		for (LineSegment e: ls) {
			res[i++] = e;
		}
		return res;
	}
}